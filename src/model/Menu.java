/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Avada Kedavra
 */
public class Menu {
    private int id;
    private String title;
    private int parent_id;

    public Menu() {
    }

    public Menu(int id, String title, int parent_id) {
        this.id = id;
        this.title = title;
        this.parent_id = parent_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    @Override
    public String toString() {
        return "Menu{" + "id=" + id + ", title=" + title + ", parent_id=" + parent_id + '}';
    }
    
    
}
